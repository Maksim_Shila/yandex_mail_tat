package com.gomel.tat.maxim_shilo.home15.framework.config;

import com.gomel.tat.maxim_shilo.home15.framework.ui.browser.config.BrowserType;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;
import org.testng.xml.XmlSuite;

import java.io.File;
import java.util.List;

import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;

public class GlobalConfig {

    @Option(name = "-bt", aliases = "--browser_type", usage = "Browser type(chrome/firefox)")
    private BrowserType browserType = BrowserType.FIREFOX;

    @Option(name = "-st", aliases = "--suites", usage = "List of paths to suites", handler = StringArrayOptionHandler.class, required = true)
    private List<String> testSuites;

    @Option(name = "-hub", usage = "Selenium hub. If null - running local web driver.")
    private String hub;

    @Option(name = "-pm", aliases = "--parallel", usage = "Parallel mode(false/tests)")
    private XmlSuite.ParallelMode parallelMode = XmlSuite.ParallelMode.FALSE;

    @Option(name = "-tc", aliases = "--thread_count", usage = "Amount of threads for parallel execution, equal to 0 by default")
    private int threadCount = 0;

    @Option(name = "-rd", aliases = "--reports_dir", usage = "Directory for reports of tests.")
    private String reportsDir = "results";

    @Option(name = "-cd", aliases = "--chrome_driver_path", usage = "Path to chrome driver.")
    private String chromeDriverPath;

    @Option(name = "-dd", aliases = "--download_directory", usage = "Default browser download directory.")
    private String downloadDirectory = "browser_downloads/";

    @Option(name = "-ts", aliases = "--take_screenshots", usage = "Take screenshot mode true/false.")
    private String screnshotEnabled = "true";

    private static GlobalConfig instance;

    public static GlobalConfig config() {
        if (instance == null) {
            synchronized (GlobalConfig.class) {
                if (instance == null) {
                    instance = new GlobalConfig();
                }
            }
        }
        return instance;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public List<String> getSuites() {
        return testSuites;
    }

    public String getHub() {
        return hub;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public XmlSuite.ParallelMode getParallelMode() {
        return parallelMode;
    }

    public String getReportsDir() {
        return reportsDir;
    }

    public String getChromeDriverPath() {
        return chromeDriverPath;
    }

    public String getDownloadDirectory() {
        File downloadDir = new File(downloadDirectory);
        if (!downloadDir.exists()) {
            logger().debug("Create directory:" + downloadDir.getAbsolutePath());
            downloadDir.mkdirs();
        }
        return downloadDir.getAbsolutePath();
    }

    public boolean isScrenshotEnabled() {
        return Boolean.parseBoolean(screnshotEnabled);
    }

    @Override
    public String toString() {
        return "GlobalConfig{" +
                "browserType=" + browserType +
                "screenshotEnabled=" + screnshotEnabled +
                ", suites=" + testSuites +
                ", parallelMode=" + parallelMode +
                ", threadCount=" + threadCount +
                ", seleniumHub='" + hub + '\'' +
                ", reportsDir='" + reportsDir + '\'' +
                '}';
    }
}
