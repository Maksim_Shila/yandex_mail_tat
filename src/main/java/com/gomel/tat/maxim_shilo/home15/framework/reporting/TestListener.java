package com.gomel.tat.maxim_shilo.home15.framework.reporting;

import org.testng.*;

import java.util.HashMap;
import java.util.Map;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.*;
import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;

public class TestListener extends TestListenerAdapter {

    private Map<Thread, String> currentTestNames = new HashMap<>();

    public String getCurrentTestName() {
        return currentTestNames.get(Thread.currentThread());
    }

    @Override
    public void onStart(ITestContext testContext) {
        currentTestNames.put(Thread.currentThread(), testContext.getCurrentXmlTest().getName());
        logger().info("");
        logger().info("==========================================================================");
        logger().info("  TEST STARTED: " + testContext.getCurrentXmlTest().getName());
        logger().info("==========================================================================");
        openBrowser();
    }

    @Override
    public void onFinish(ITestContext testContext) {
        logger().info("==========================================================================");
        logger().info("  TEST FINISHED: " + testContext.getCurrentXmlTest().getName());
        logger().info("  Test methods run: " + testContext.getAllTestMethods().length +
                ", Failures: " + testContext.getFailedTests().size() + ", Skips: " + testContext.getSkippedTests().size());
        if (testContext.getFailedConfigurations().size() > 0) {
            logger().info("  Configuration failures: " + testContext.getFailedConfigurations().size());
        }
        logger().info("==========================================================================");
        logger().info("");
        closeBrowser();
    }

    @Override
    public void onTestFailure(ITestResult testRes) {
        logger().error("Test method failed: " + testRes.getMethod().getDescription(), testRes.getThrowable());
        browser().getScreenshot();
    }
}
