package com.gomel.tat.maxim_shilo.home15.framework.runner;

import com.gomel.tat.maxim_shilo.home15.lib.feature.steps.LoginSteps;
import com.gomel.tat.maxim_shilo.home15.lib.feature.steps.MailSteps;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.*;

public class StoryRunner extends JUnitStories {

    @Test(description = "Run stories")
    @Override
    public void run() throws Throwable {
        super.run();
        closeBrowser();
    }

    @Override
    protected List<String> storyPaths() {
        List<String> paths = new ArrayList<String>() {{
            add("stories/login_stories/SuccessLogin.story");
            add("stories/login_stories/WrongPasswordLogin.story");
            add("stories/login_stories/NonExistingAccountLogin.story");
            add("stories/mail_stories/SendMail.story");
        }};
        return paths;
    }

    @Override
    public Configuration configuration() {
        return new MostUsefulConfiguration().useStoryLoader(new LoadFromClasspath())
                .usePendingStepStrategy(new FailingUponPendingStep())
                .useStoryReporterBuilder(new StoryReporterBuilder().withDefaultFormats().withFormats(Format.CONSOLE, Format.TXT));
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        List<Object> stepInstances = new ArrayList<Object>() {{
            add(new LoginSteps());
            add(new MailSteps());
        }};
        return new InstanceStepsFactory(configuration(), stepInstances);
    }
}
