package com.gomel.tat.maxim_shilo.home15.framework.ui.browser;

import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;
import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.config.BrowserStack.*;
import static java.lang.Thread.*;

public class Browser extends BrowserFunctional {

    private Browser() {
    }

    public static void openBrowser() {
        try {
            browserStack().put(new Browser(), currentThread());
        } catch (Exception e) {
            logger().error("Failed to open browser", e);
        }
    }

    public static void closeBrowser() {
        try {
            browserStack().close(currentThread());
        } catch (Exception e) {
            logger().error("Failed to close browser", e);
        }
    }

    public static Browser browser() {
        return browserStack().get(currentThread());
    }
}