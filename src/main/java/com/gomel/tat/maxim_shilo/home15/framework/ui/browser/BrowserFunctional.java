package com.gomel.tat.maxim_shilo.home15.framework.ui.browser;

import com.gomel.tat.maxim_shilo.home15.framework.ui.browser.config.*;
import com.gomel.tat.maxim_shilo.home15.framework.ui.driver.Driver;
import com.gomel.tat.maxim_shilo.home15.framework.ui.driver.factory.DriverFactory;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home15.framework.config.GlobalConfig.config;

public abstract class BrowserFunctional {

    private static final BrowserType browserType = config().getBrowserType();
    private static final String hub = config().getHub();

    private Driver driver;
    private BrowserFunctionalFactory functionalFactory;

    public BrowserFunctional() {
        driver = new Driver(browserType, hub);
        functionalFactory = new BrowserFunctionalFactory(driver);
    }

    public WebDriver getWebDriver() {
        return driver.getWebDriver();
    }

    public String getDownloadDir() {
        return DriverFactory.getDownloadDir();
    }

    public void open(String URL) {
        functionalFactory.pageActions().open(URL);
    }

    public WebElement findElement(By locator) {
        return functionalFactory.pageActions().findElement(locator);
    }

    public void waitForElementIsPresent(Element element) {
        functionalFactory.browserWait().forElementIsPresent(element);
    }

    public void waitForElementIsDisappear(Element element) {
        functionalFactory.browserWait().forElementIsDisappear(element);
    }

    public void waitForElementIsClickable(Element element) {
        functionalFactory.browserWait().forElementIsClickable(element);
    }

    public void waitForElementIsVisible(Element element) {
        functionalFactory.browserWait().forElementIsVisible(element);
    }

    public void moveToElement(Element element) {
        functionalFactory.actions().moveToElement(element);
    }

    public void dragAndDrop(Element draggable, Element to) {
        functionalFactory.actions().dragAndDrop(draggable, to);
    }

    public void getScreenshot() {
        functionalFactory.pageActions().getScreenshot();
    }

    public void delay(int seconds) {
        functionalFactory.browserWait().delay(seconds);
    }

    public void selectElementsUsingCTRL(ArrayList<Element> elements) {
        functionalFactory.actions().selectElementsUsingCTRL(elements);
    }

    public void scrollWindow(int scrollOffset) {
        functionalFactory.js().scrollWindow(scrollOffset);
    }
}
