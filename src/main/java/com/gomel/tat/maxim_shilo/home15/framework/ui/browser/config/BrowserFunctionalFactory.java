package com.gomel.tat.maxim_shilo.home15.framework.ui.browser.config;

import com.gomel.tat.maxim_shilo.home15.framework.ui.browser.config.functional.*;
import com.gomel.tat.maxim_shilo.home15.framework.ui.driver.Driver;

public class BrowserFunctionalFactory {

    private Driver driver;

    public BrowserFunctionalFactory(Driver driver) {
        this.driver = driver;
    }

    public BrowserActions actions() {
        return new BrowserActions(driver);
    }

    public BrowserJS js() {
        return new BrowserJS(driver);
    }

    public BrowserWaits browserWait() {
        return new BrowserWaits(driver);
    }

    public BrowserPageActions pageActions() {
        return new BrowserPageActions(driver);
    }
}
