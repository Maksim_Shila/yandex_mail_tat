package com.gomel.tat.maxim_shilo.home15.framework.ui.browser.config;

import com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

public class BrowserStack {

    private BrowserStack() {
    }

    private static BrowserStack stackInstance;
    private static Map<Thread, Browser> browserInstances = new HashMap<>();

    public static BrowserStack browserStack() {
        if (stackInstance == null){
            stackInstance = new BrowserStack();
        }
        return stackInstance;
    }

    public void put(Browser browser, Thread thread) throws Exception {
        if (browserInstances.get(thread) != null) {
            close(thread);
        }
        browserInstances.put(Thread.currentThread(), browser);
    }

    public synchronized Browser get(Thread thread) {
        if (browserInstances.get(thread) == null) {
            throw new SessionNotCreatedException("Browser not opened!");
        }
        return browserInstances.get(Thread.currentThread());
    }

    public void close(Thread thread) throws Exception {
        WebDriver driver = browserInstances.get(thread).getWebDriver();
        if (driver != null) {
            driver.quit();
        }
        browserInstances.put(thread, null);
    }
}
