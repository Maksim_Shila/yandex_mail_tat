package com.gomel.tat.maxim_shilo.home15.framework.ui.browser.config.functional;

import com.gomel.tat.maxim_shilo.home15.framework.ui.driver.Driver;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Element;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.browser;

public class BrowserActions {

    private Actions builder;

    public BrowserActions(Driver driver) {
        builder = new Actions(driver.getWebDriver());
    }

    public void selectElementsUsingCTRL(ArrayList<Element> elements) {
        builder.keyDown(Keys.CONTROL);
        for (Element element : elements) {
            builder.moveToElement(browser().findElement(element.getBy()))
                    .click(browser().findElement(element.getBy()));
        }
        builder.build().perform();
    }

    public void dragAndDrop(Element draggable, Element to) {
        builder.moveToElement(browser().findElement(draggable.getBy()))
                .clickAndHold(browser().findElement(draggable.getBy()))
                .moveToElement(browser().findElement(to.getBy()))
                .release()
                .build().perform();
    }

    public void moveToElement(Element element) {
        builder.moveToElement(browser().findElement(element.getBy())).perform();
    }
}
