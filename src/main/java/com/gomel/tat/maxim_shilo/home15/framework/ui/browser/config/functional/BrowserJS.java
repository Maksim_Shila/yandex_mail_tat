package com.gomel.tat.maxim_shilo.home15.framework.ui.browser.config.functional;

import com.gomel.tat.maxim_shilo.home15.framework.ui.driver.Driver;
import org.openqa.selenium.JavascriptExecutor;

public class BrowserJS {

    private JavascriptExecutor executor;

    public BrowserJS(Driver driver) {
        executor = (JavascriptExecutor) driver.getWebDriver();
    }

    public void scrollWindow(int offset) {
        executor.executeScript(String.format("window.scrollBy(0,%s)", offset), "");
    }
}
