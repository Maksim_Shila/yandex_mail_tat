package com.gomel.tat.maxim_shilo.home15.framework.ui.browser.config.functional;

import com.gomel.tat.maxim_shilo.home15.framework.ui.driver.Driver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.io.IOException;

import static com.gomel.tat.maxim_shilo.home15.framework.config.GlobalConfig.config;
import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;
import static java.io.File.separator;
import static java.lang.System.nanoTime;

public class BrowserPageActions {

    private Driver driver;
    public BrowserPageActions(Driver driver) {
        this.driver = driver;
    }

    public void open(String URL) {
        driver.getWebDriver().get(URL);
    }

    public WebElement findElement(By locator) {
        try {
            return driver.getWebDriver().findElement(locator);
        } catch (StaleElementReferenceException e) {
            return driver.getWebDriver().findElement(locator);
        }
    }

    public void getScreenshot() {
        if (config().isScrenshotEnabled()) {
            try {
                File sreenshotFile = new File(config().getReportsDir() + separator + nanoTime() + ".png");
                File scrFile = ((TakesScreenshot) driver.getWebDriver()).getScreenshotAs(OutputType.FILE);
                logger().save(sreenshotFile.getName());
                FileUtils.copyFile(scrFile, sreenshotFile);
            } catch (IOException e) {
                logger().error(e.getMessage(), e);
            }
        }
    }
}
