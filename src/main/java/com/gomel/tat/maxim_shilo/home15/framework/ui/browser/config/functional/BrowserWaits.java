package com.gomel.tat.maxim_shilo.home15.framework.ui.browser.config.functional;

import com.gomel.tat.maxim_shilo.home15.framework.ui.driver.Driver;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Element;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class BrowserWaits {

    private static final int DEFAULT_TIMEOUT_SECONDS = 30;
    private static final int TIMEOUT_ELEMENT_NOT_PRESENT_SECONDS = 20;

    private Driver driver;

    public BrowserWaits(Driver driver) {
        this.driver = driver;
    }

    private WebDriverWait wait(int timeout) {
        return new WebDriverWait(driver.getWebDriver(), timeout);
    }

    public void forElementIsPresent(Element element) {
        wait(DEFAULT_TIMEOUT_SECONDS).until(presenceOfElementLocated(element.getBy()));
    }

    public void forElementIsClickable(Element element) {
        wait(DEFAULT_TIMEOUT_SECONDS).until(elementToBeClickable(element.getBy()));
    }

    public void forElementIsVisible(Element element) {
        wait(DEFAULT_TIMEOUT_SECONDS).until(visibilityOfElementLocated(element.getBy()));
    }

    public void forElementIsDisappear(Element element) {
        for (int i = 0; i < TIMEOUT_ELEMENT_NOT_PRESENT_SECONDS; i++) {
            try {
                element.findWebElement();
            } catch (NoSuchElementException e) {
                return;
            }
            delay(500);
        }
        throw new RuntimeException("Element still present.");
    }

    public void delay(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            logger().error(e.getMessage(), e);
        }
    }
}
