package com.gomel.tat.maxim_shilo.home15.framework.ui.driver;

import com.gomel.tat.maxim_shilo.home15.framework.ui.browser.config.BrowserType;
import com.gomel.tat.maxim_shilo.home15.framework.ui.driver.factory.*;
import org.openqa.selenium.WebDriver;

import java.net.MalformedURLException;

import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;

public class Driver {

    private static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 10;
    private static final int DRIVER_IMPLICITY_WAIT_TIMEOUT_SECONDS = 4;

    private WebDriver driver;
    private DriverFactory driverFactory;

    private BrowserType browserType;
    private String hub;

    public Driver(BrowserType browserType, String hub) {
        this.browserType = browserType;
        this.hub = hub;
        initDriverFactory();
        initWebDriver();
        setupDriver();
    }

    private void initDriverFactory() {
        switch (browserType) {
            case CHROME:
                driverFactory = new ChromeDriverFactory();
                break;
            case FIREFOX:
                driverFactory = new FirefoxDriverFactory();
                break;
            default:
                throw new IllegalArgumentException("Cannot init driverFactory in case lack of browser " + browserType);
        }
    }

    private void initWebDriver() {
        try {
            if (hub == null) {
                driver = driverFactory.getLocalWebDriver();
            } else {
                driver = driverFactory.getRemoteWebDriver();
            }
        } catch (MalformedURLException e) {
            logger().error(e.getMessage(), e);
        }
    }

    private void setupDriver() {
        driverFactory.setFileDetector(driver);
        driverFactory.setImplicityWaitTimeout(driver, DRIVER_IMPLICITY_WAIT_TIMEOUT_SECONDS);
        driverFactory.setPageLoadTimeout(driver, DRIVER_PAGE_LOAD_TIMEOUT_SECONDS);
        driverFactory.maximizeWinwow(driver, true);
    }

    public WebDriver getWebDriver() {
        return driver;
    }
}
