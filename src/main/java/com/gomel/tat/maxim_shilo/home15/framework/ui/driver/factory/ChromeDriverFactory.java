package com.gomel.tat.maxim_shilo.home15.framework.ui.driver.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.gomel.tat.maxim_shilo.home15.framework.config.GlobalConfig.config;
import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;

public class ChromeDriverFactory extends DriverFactory {

    private static final String PATH_TO_CHROME_DRIVER = config().getChromeDriverPath();

    public WebDriver getLocalWebDriver() {
        logger().debug("Create local chrome driver.");
        System.setProperty("webdriver.chrome.driver", PATH_TO_CHROME_DRIVER);
        return new ChromeDriver(getChromeOptions());
    }

    public WebDriver getRemoteWebDriver() throws MalformedURLException {
        logger().debug("Create remote chrome driver.");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, getChromeOptions());
        return new RemoteWebDriver(new URL(SELENIUM_HUB), capabilities);
    }

    private ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("download.default_directory", DOWNLOAD_DIR);
        options.setExperimentalOption("prefs", prefs);
        options.addArguments("--lang=en-us");
        return options;
    }
}
