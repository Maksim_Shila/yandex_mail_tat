package com.gomel.tat.maxim_shilo.home15.framework.ui.driver.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;

import static com.gomel.tat.maxim_shilo.home15.framework.config.GlobalConfig.config;
import static java.util.concurrent.TimeUnit.SECONDS;

public abstract class DriverFactory {

    protected static final String DOWNLOAD_DIR = config().getDownloadDirectory();
    protected static final String SELENIUM_HUB = config().getHub();

    public abstract WebDriver getLocalWebDriver();

    public abstract WebDriver getRemoteWebDriver() throws MalformedURLException;

    public void setFileDetector(WebDriver driver) {
        if (config().getHub() != null) {
            ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
        }
    }

    public void setImplicityWaitTimeout(WebDriver driver, int seconds) {
        driver.manage().timeouts().implicitlyWait(seconds, SECONDS);
    }

    public void maximizeWinwow(WebDriver driver, boolean maximize) {
        if (maximize) {
            driver.manage().window().maximize();
        }
    }

    public void setPageLoadTimeout(WebDriver driver, int seconds) {
        driver.manage().timeouts().pageLoadTimeout(seconds, SECONDS);
    }

    public static String getDownloadDir() {
        return DOWNLOAD_DIR;
    }
}
