package com.gomel.tat.maxim_shilo.home15.framework.ui.driver.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;

public class FirefoxDriverFactory extends DriverFactory {

    public WebDriver getLocalWebDriver() {
        logger().debug("Create local firefox driver.");
        return new FirefoxDriver(getFirefoxProfile());
    }

    public WebDriver getRemoteWebDriver() throws MalformedURLException {
        logger().debug("Create remote firefox driver.");
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.PROFILE, getFirefoxProfile());
        return new RemoteWebDriver(new URL(SELENIUM_HUB), capabilities);
    }

    private FirefoxProfile getFirefoxProfile() {
        FirefoxProfile ffProfile = new FirefoxProfile();
        ffProfile.setPreference("browser.download.folderList", 2);
        ffProfile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        ffProfile.setPreference("browser.download.manager.showWhenStarting", false);
        ffProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        return ffProfile;
    }
}
