package com.gomel.tat.maxim_shilo.home15.framework.ui.elements;

import com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.*;

public class Button extends Element {

    public Button(String xpath) {
        super(xpath);
    }

    public void click() {
        CustomLogger.logger().debug("Click button located: " + getBy());
        browser().getScreenshot();
        browser().waitForElementIsClickable(this);
        findWebElement().click();
    }
}
