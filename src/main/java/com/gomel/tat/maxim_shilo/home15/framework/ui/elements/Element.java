package com.gomel.tat.maxim_shilo.home15.framework.ui.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.*;
import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;

public class Element {

    private By element;
    private String xpath;

    public Element(String xpath) {
        element = By.xpath(xpath);
        this.xpath = xpath;
    }

    public By getBy() {
        return element;
    }

    protected String getXpath() {
        return xpath;
    }

    public WebElement findWebElement() {
        browser().getScreenshot();
        logger().debug("Find web element: " + this);
        return browser().findElement(getBy());
    }

    public boolean isPresent() {
        logger().debug("Check that element present: " + this);
        try {
            browser().waitForElementIsPresent(this);
        } catch (RuntimeException e) {
            browser().getScreenshot();
            return false;
        }
        browser().getScreenshot();
        return true;
    }

    public boolean isNotPresent() {
        logger().debug("Check that element not present: " + this);
        try {
            browser().waitForElementIsDisappear(this);
        } catch (RuntimeException e) {
            browser().getScreenshot();
            return false;
        }
        browser().getScreenshot();
        return true;
    }

    public String getText() {
        logger().debug("Get text from element located: " + this);
        browser().getScreenshot();
        return findWebElement().getText();
    }

    public Element focus() {
        logger().debug("Focus on element: " + this);
        browser().waitForElementIsPresent(this);
        if (this instanceof Input){
            this.findWebElement().click();
        } else {
            browser().moveToElement(this);
        }
        return this;
    }

    @Override
    public String toString() {
        return element.toString();
    }
}
