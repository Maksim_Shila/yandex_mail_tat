package com.gomel.tat.maxim_shilo.home15.framework.ui.elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;

public class Form extends Element {

    public Form(String xpath) {
        super(xpath);
    }

    private Map<String, Input> formInputs = new HashMap<>();

    public Form addInputs(ArrayList<String> names) {
        for (String name : names) {
            formInputs.put(name, new Input(String.format(this.getXpath() + "//input[@name='%s']", name)));
        }
        return this;
    }

    public Input input(String name) {
        return formInputs.get(name);
    }

    public void submit() {
        logger().debug("Submit form: " + this);
        findWebElement().submit();
    }
}
