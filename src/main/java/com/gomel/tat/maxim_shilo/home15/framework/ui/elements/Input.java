package com.gomel.tat.maxim_shilo.home15.framework.ui.elements;

import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;
import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.browser;

public class Input extends Element {

    public Input(String xpath) {
        super(xpath);
    }

    public void type(String text) {
        logger().debug("Write text: " + text + ", to: " + getBy());
        browser().getScreenshot();
        findWebElement().sendKeys(text);
    }
}
