package com.gomel.tat.maxim_shilo.home15.framework.ui.elements;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.browser;
import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;

public class Link extends Element {

    public Link(String xpath) {
        super(xpath);
    }

    public void click() {
        logger().debug("Click link: " + getBy());
        browser().getScreenshot();
        findWebElement().click();
    }
}
