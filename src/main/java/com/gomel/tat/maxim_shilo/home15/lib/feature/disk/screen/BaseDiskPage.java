package com.gomel.tat.maxim_shilo.home15.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Link;

import java.util.NoSuchElementException;

public abstract class BaseDiskPage {

    private static final String UPLOADED_FILE_LOCATOR_PATTERN = "//div[contains(@data-id,'%s')]";

    public abstract BaseDiskPage open();

    public Link file(YaFile yaFile) {
        return new Link(String.format(UPLOADED_FILE_LOCATOR_PATTERN, yaFile));
    }

    public void selectFile(YaFile yandexFile) {
        if (isFilePresent(yandexFile)) {
            ((Link) file(yandexFile).focus()).click();
        } else {
            throw new NoSuchElementException("File " + yandexFile + " is not present!");
        }
    }

    public boolean isFilePresent(YaFile yaFile) {
        return file(yaFile).isPresent();
    }

    public boolean isFileNotPresent(YaFile yaFile) {
        return file(yaFile).isNotPresent();
    }
}
