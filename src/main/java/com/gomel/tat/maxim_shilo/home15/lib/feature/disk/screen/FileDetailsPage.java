package com.gomel.tat.maxim_shilo.home15.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Button;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.browser;

public class FileDetailsPage {

    private static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.download' and contains(@data-params,'%s')]";

    private Button download;

    public FileDetailsPage(YaFile yandexFile) {
        download = new Button(String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, yandexFile));
    }

    public FileDetailsPage clickDownloadButton() {
        ((Button)download.focus()).click();
        return this;
    }

    public FileDetailsPage waitForPageLoad() {
        browser().waitForElementIsVisible(download);
        return this;
    }
}
