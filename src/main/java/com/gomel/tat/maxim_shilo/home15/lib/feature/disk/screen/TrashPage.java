package com.gomel.tat.maxim_shilo.home15.lib.feature.disk.screen;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.browser;

public class TrashPage extends BaseDiskPage {

    private static final String TRASH_PAGE = "https://disk.yandex.com/client/trash";

    public TrashPage open() {
        browser().open(TRASH_PAGE);
        return this;
    }
}
