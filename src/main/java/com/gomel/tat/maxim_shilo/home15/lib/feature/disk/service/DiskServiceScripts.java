package com.gomel.tat.maxim_shilo.home15.lib.feature.disk.service;

import com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home15.lib.feature.login.service.LoginService;

import static com.gomel.tat.maxim_shilo.home15.lib.feature.common.AccountFactory.getAccount;

public class DiskServiceScripts {

    private DiskService diskService = new DiskService();

    public void prepareFileInTrash(YaFile yandexFile){
        new LoginService().loginToDisk(getAccount());
        diskService.uploadFile(yandexFile);
        diskService.isFilePresent(yandexFile);
        diskService.deleteFile(yandexFile);
        diskService.openTrash();
    }

    public void prepareFileOnDisk(YaFile yandexFile){
        new LoginService().loginToDisk(getAccount());
        diskService.uploadFile(yandexFile);
        diskService.isFilePresent(yandexFile);
    }
}
