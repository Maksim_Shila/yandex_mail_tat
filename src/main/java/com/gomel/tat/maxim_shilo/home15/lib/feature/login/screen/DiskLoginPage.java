package com.gomel.tat.maxim_shilo.home15.lib.feature.login.screen;

import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Form;
import com.gomel.tat.maxim_shilo.home15.lib.feature.disk.screen.DiskPage;

import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.browser;

public class DiskLoginPage {

    private static final String YA_DISK_URL = "https://disk.yandex.com";

    private static final String loginInputName = "login";
    private static final String passwordInputName = "password";

    private Form loginForm = new Form("//form[contains(@class,'login')]")
            .addInputs(new ArrayList<String>() {{
                add(loginInputName);
                add(passwordInputName);
            }});

    public DiskLoginPage open() {
        browser().open(YA_DISK_URL);
        return this;
    }

    public DiskLoginPage typeLogin(String userName) {
        loginForm.input(loginInputName).type(userName);
        return this;
    }

    public DiskLoginPage typePassword(String userPassword) {
        loginForm.input(passwordInputName).type(userPassword);
        return this;
    }

    public DiskPage submitLoginForm() {
        loginForm.submit();
        return new DiskPage();
    }
}
