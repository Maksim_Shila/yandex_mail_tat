package com.gomel.tat.maxim_shilo.home15.lib.feature.login.service;

import com.gomel.tat.maxim_shilo.home15.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home15.lib.feature.login.screen.DiskLoginPage;
import com.gomel.tat.maxim_shilo.home15.lib.feature.mail.screen.BaseMailPage;
import com.gomel.tat.maxim_shilo.home15.lib.feature.login.screen.MailLoginPage;
import com.gomel.tat.maxim_shilo.home15.lib.feature.mail.screen.LoginFailedPage;

import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;

public class LoginService {

    private MailLoginPage mailLoginPage = new MailLoginPage();
    private DiskLoginPage diskLoginPage = new DiskLoginPage();

    public void loginToMailbox(Account account) {
        logger().info("Try login to mail box using credentials [" + account.getLogin() + "::" + account.getPassword() + "]");
        mailLoginPage.open()
                .typeLogin(account.getLogin())
                .typePassword(account.getPassword())
                .submitLoginForm();
    }

    public void loginToDisk(Account account){
        logger().info("Try login to disk using credentials [" + account.getLogin() + "::" + account.getPassword() + "]");
        diskLoginPage.open()
                .typeLogin(account.getLogin())
                .typePassword(account.getPassword())
                .submitLoginForm();
    }

    public boolean isLoginSuccess(Account account) {
        logger().info("Check that login success using credentials " + account.getLogin() + "::" + account.getPassword());
        return new BaseMailPage().isOpenedForAccount(account);
    }

    public String retrieveErrorOnFailedLogin() {
        logger().info("Retrieving error message.");
        return new LoginFailedPage().getErrorMessage();
    }
}
