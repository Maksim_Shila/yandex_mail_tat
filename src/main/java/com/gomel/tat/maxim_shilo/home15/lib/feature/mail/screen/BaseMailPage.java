package com.gomel.tat.maxim_shilo.home15.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home15.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home15.lib.feature.mail.bo.Letter;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Button;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Element;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Link;

import java.util.NoSuchElementException;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.browser;

public class BaseMailPage {

    private static final String MAIL_LINK_LOCATOR_PATTERN =
            "//*[@class='block-messages' and (not(@style) or @style=\"\")]//*[@title='%s']";

    private Button compose = new Button("//a[@href='#compose']");
    private Element userDropdownMenu = new Element("//*[@id='nb-1']/span[contains(@class,'user-name')]");
    private Element statusline = new Element("//span[@class='b-statusline__content' and contains(text(),'message deleted')]");
    private Button closeStatusLine = new Button("//div[@class='b-statusline']//span[@title='Hide message']");

    private Link uniqueLetter(Letter letter) {
        return new Link(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject()));
    }

    public boolean isOpenedForAccount(Account account) {
        browser().waitForElementIsPresent(userDropdownMenu);
        return userDropdownMenu.getText().equals(account.getEmail());
    }

    public FoldersMenu openFolder(FoldersMenu folderPage) {
        folderPage.getFolderButton().click();
        return folderPage;
    }

    public ComposePage clickComposeLetterButton() {
        compose.click();
        return new ComposePage();
    }

    public void closeMessageLetterDeleted() {
        browser().waitForElementIsVisible(statusline);
        closeStatusLine.click();
    }

    public void openLetter(Letter letter) {
        if (isLetterPresent(letter)) {
            uniqueLetter(letter).click();
        } else {
            throw new NoSuchElementException("Letter " + letter + " is not present!");
        }
    }

    public boolean isLetterPresent(Letter letter) {
        return uniqueLetter(letter).isPresent();
    }

    public boolean isLetterNotPresent(Letter letter) {
        return uniqueLetter(letter).isNotPresent();
    }
}
