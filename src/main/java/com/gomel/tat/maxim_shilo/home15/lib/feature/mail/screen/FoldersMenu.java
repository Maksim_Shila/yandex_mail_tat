package com.gomel.tat.maxim_shilo.home15.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Button;

public abstract class FoldersMenu extends BaseMailPage {

    public abstract Button getFolderButton();
}
