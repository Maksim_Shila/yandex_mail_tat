package com.gomel.tat.maxim_shilo.home15.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Button;

public class LetterPage {

    private Button delete = new Button("//a[@data-action='delete']");

    public BaseMailPage clickDeleteLetterButton() {
        delete.click();
        return new BaseMailPage();
    }
}
