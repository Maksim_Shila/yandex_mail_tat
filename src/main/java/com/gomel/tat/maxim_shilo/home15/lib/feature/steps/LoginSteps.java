package com.gomel.tat.maxim_shilo.home15.lib.feature.steps;

import com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser;
import com.gomel.tat.maxim_shilo.home15.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home15.lib.feature.login.service.LoginService;
import org.jbehave.core.annotations.*;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.browser.Browser.*;
import static com.gomel.tat.maxim_shilo.home15.lib.feature.common.AccountFactory.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class LoginSteps {

    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS = "Wrong username or password.";
    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_ACCOUNT_NOT_EXIST = "No such account with this username.";

    private Account actor;
    private LoginService loginService = new LoginService();

    @BeforeScenario
    public void openBrowser(){
        Browser.openBrowser();
    }

    @AfterScenario
    public void quitBrowser(){
        closeBrowser();
    }

    @Given("Actor is mailbox owner")
    public void buildAccount() {
        actor = getAccount();
    }

    @Given("Actor has invalid password")
    public void accountWithInvalidPassword() {
        actor = getAccountWithWrongPassword();
    }

    @Given("Actor has non-existing account")
    @Aliases(values = {"Actor has account that not exists."})
    public void nonExistingAccount(){
        actor = getNonExistedAccount();
    }

    @When("Actor open login page and enter credentials and click submit")
    @Aliases(values = {"Actor login to mailbox", ""})
    public void login() {
       loginService.loginToMailbox(actor);
    }

    @Then("Actor's Mailbox page is opened")
    public void checkThatMailboxOpened() {
        assertThat("Opened mailbox is not actor", loginService.isLoginSuccess(actor));
    }

    @Then("Actor get page with error message in case wrong password.")
    public void checkErrorInCaseWrongPassword(){
        checkThatErrorMessagePresent(EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS);
    }

    @Then("Actor get page with error message in case account not exists.")
    public void checkErrorInCaseNonExistingAccount(){
        checkThatErrorMessagePresent(EXPECTED_ERROR_MESSAGE_IN_CASE_ACCOUNT_NOT_EXIST);
    }

    public void checkThatErrorMessagePresent(String message) {
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin();
        assertThat("Error present but actual and expected message not equals."
                , actualErrorMessage, equalTo(message));
    }
}
