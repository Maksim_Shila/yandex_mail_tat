package com.gomel.tat.maxim_shilo.home15.tests.login;

import com.gomel.tat.maxim_shilo.home15.lib.feature.login.service.LoginService;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home15.lib.feature.common.AccountFactory.getAccountWithWrongPassword;
import static com.gomel.tat.maxim_shilo.home15.lib.feature.common.AccountFactory.getNonExistedAccount;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class NegativeLoginTest {

    private static final String ASSERT_FAIL_MESSAGE = "Error present but actual and expected message not equals.";
    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS = "Wrong username or password.";
    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_ACCOUNT_NOT_EXIST = "No such account with this username.";

    private LoginService loginService = new LoginService();

    @Test(description = "Check expected error message in case of wrong password")
    public void checkErrorMessageWrongPassword() {
        loginService.loginToMailbox(getAccountWithWrongPassword());
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin();
        assertThat(ASSERT_FAIL_MESSAGE, actualErrorMessage, equalTo(EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS));
    }

    @Test(description = "Check expected error in case of non existed account")
    public void checkErrorMessageNonExistedAccount() {
        loginService.loginToMailbox(getNonExistedAccount());
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin();
        assertThat(ASSERT_FAIL_MESSAGE, actualErrorMessage, equalTo(EXPECTED_ERROR_MESSAGE_IN_CASE_ACCOUNT_NOT_EXIST));
    }
}
