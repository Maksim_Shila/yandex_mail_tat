package com.gomel.tat.maxim_shilo.home15.tests.login;

import com.gomel.tat.maxim_shilo.home15.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home15.lib.feature.login.service.LoginService;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home15.lib.feature.common.AccountFactory.getAccount;
import static java.lang.String.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class SuccessLoginTest {

    private Account account = getAccount();
    private LoginService loginService = new LoginService();
    private static final String ASSERT_FAIL_MESSAGE_PATTERN = "Login failed with credentials:\n%s";

    @Test(description = "Login into mail as valid user.")
    public void loginAndCheckThatLoginSuccess() {
        loginService.loginToMailbox(account);
        assertThat(format(ASSERT_FAIL_MESSAGE_PATTERN, account), loginService.isLoginSuccess(account));
    }
}
