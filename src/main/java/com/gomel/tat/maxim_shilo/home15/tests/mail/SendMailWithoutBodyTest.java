package com.gomel.tat.maxim_shilo.home15.tests.mail;

import com.gomel.tat.maxim_shilo.home15.lib.feature.mail.bo.Letter;
import com.gomel.tat.maxim_shilo.home15.lib.feature.login.service.LoginService;
import com.gomel.tat.maxim_shilo.home15.lib.feature.mail.service.MailService;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home15.lib.feature.common.AccountFactory.getAccount;
import static com.gomel.tat.maxim_shilo.home15.lib.feature.mail.bo.LetterFactory.getLetterWithoutBodyAndSubject;
import static org.hamcrest.MatcherAssert.assertThat;

public class SendMailWithoutBodyTest {

    private Letter letter = getLetterWithoutBodyAndSubject();
    private MailService mailService = new MailService();
    private boolean mailPresentInInbox = false;
    private boolean mailPresentInOutbox = false;

    private static final String SEND_MAIL_ASSERT_FAIL_MESSAGE = "Empty mail not sended.";
    private static final String CHECK_INBOX_ASSERT_FAIL_MESSAGE = "Empty mail is not present in inbox folder.";
    private static final String CHECK_OUTBOX_ASSERT_FAIL_MESSAGE = "Empty mail is not present in outbox folder.";

    @BeforeClass(description = "Login to mail.")
    public void login() {
        new LoginService().loginToMailbox(getAccount());
    }

    @Test(description = "Send mail with empty subject field and body.")
    public void sendMailWithoutBodyAndSubject() {
        mailService.sendEmptyMail(letter.getRecipient());
        assertThat(SEND_MAIL_ASSERT_FAIL_MESSAGE, mailService.isLetterSent());
    }

    @Test(dependsOnMethods = "sendMailWithoutBodyAndSubject", description = "Check that mail is present in inbox folder.")
    public void checkInbox() {
        mailService.backToInbox();
        assertThat(CHECK_INBOX_ASSERT_FAIL_MESSAGE, mailService.isLetterPresent(letter));
        mailPresentInInbox = true;
    }

    @Test(dependsOnMethods = "sendMailWithoutBodyAndSubject", description = "Check that mail is present in outbox folder.")
    public void checkOutbox() {
        mailService.openOutboxFolder();
        assertThat(CHECK_OUTBOX_ASSERT_FAIL_MESSAGE, mailService.isLetterPresent(letter));
        mailPresentInOutbox = true;
    }

    @AfterClass()
    public void deleteEmptyLetters() {
        if (mailPresentInInbox) {
            mailService.openInboxFolder();
            mailService.deleteLetter(letter);
        }
        if (mailPresentInOutbox) {
            mailService.openOutboxFolder();
            mailService.deleteLetter(letter);
        }
    }
}