Meta:

Narrative:
As an actor
I want try login to mailbox with wrong password
So that I can't get access to mailbox and correct error message present.

Scenario: scenario description
Given Actor has invalid password
When Actor login to mailbox
Then Actor get page with error message in case wrong password.