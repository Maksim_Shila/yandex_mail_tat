Meta:

Narrative:
As an actor
I want to send mail to myself
So that I can send mail and get it.

Scenario: scenario description
Given Actor is mailbox owner
And Letter that will be sent
When Actor login to mailbox
And Actor send mail to himself
Then Actor get page letter sent
And Actor get mail in inbox folder
And Actor get mail in outbox folder